# routes/tasks.rb

get '/api/tasks' do 
  Task.all.to_json
end

get '/api/tasks/:id' do 
  task = Task.get(params[:id])
  if task.nil?
    halt 404
  end
  task.to_json
end

post '/api/tasks' do 
  body = JSON.parse request.body.read
  task = Task.create(
    title: body['title'],
    director: body['director']
    year: body['year']
    )

  status 201
  task.to_json
end

put '/api/tasks/:id' do 
  body = JSON.parse request.body.read
  task = Task.get(params[:id])

  if task.nil?
    halt 404
  end

  halt 500 unless task.update(
    title: body['title'],
    director: body['director'],
    year: body['year']
    )
  task.to_json
end

delete '/api/tasks/:id' do 
  task = Task.get(params[:id])

  if task.nil?
    halt 404
  end

  halt 500 unless task.destroy
end